const express = require('express');
require('dotenv').config();
const cors = require('cors');


class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT || 3000;
        this.userPath = '/api/user';

        //middlewares
        this.middlewares();

        //bodyparser
        this.app.use(express.json());

        //routes
        this.start();
    }

    middlewares() {
        this.app.use(cors());
    }

    start() {
        this.app.use(this.userPath, require('../routes/user.js'));
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log(`Example app listening on port ${this.port}!`);
        });
    }
}


module.exports = Server;