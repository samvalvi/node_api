const { response} = require('express');

var users = [
    {
        id: 1,
        name: 'John Doe',
        email: 'john.doe@gmail.com',
        city: 'New York',
        country: 'USA',
        createdAt: '2020-01-01',
        age: 30
    },
    {
        id: 2,
        name: 'Jane Doe',
        email: 'jane.doe@gmail.com',
        city: 'New York',
        country: 'USA',
        createdAt: '2020-01-01',
        age: 20
    },
    {
        id: 3,
        name: 'Annie Doe',
        email: 'annie.doe@gmail.com',
        city: 'New York',
        country: 'USA',
        createdAt: '2020-01-01',
        age: 10
    }
]

const getUsers = (req, res = response) => {
    res.json({
        statusCode: 200,
        message: 'Users founded',
        data: {
            users
        }
    })
};


const getUserById = (req, res = response) => {
   const userId = parseInt(req.params.id);
    const result = users.find(user => user.id === userId);
    
    if(!result) {
        return res.status(404).json({
            statusCode: 404,
            message: 'User not found'
        })
    }
    res.status(200).json({
        statusCode: 200,
        message: 'User founded',
        data: {
            result
        }
    })
};


const createUser = (req, res = response) => {
    const body = req.body;

    if(!body.name || !body.email || !body.city || !body.country || !body.age) {
        return res.status(400).json({
            statusCode: 400,
            message: 'All fields are required'
        })
    }

    const newUser = {
        id: users.length + 1,
        name: body.name,
        email: body.email,
        city: body.city,
        country: body.country,
        createdAt: new Date(),
        age: body.age
    }

    users.push(newUser);

    res.status(201).json({
        statusCode: 201,
        message: 'User created',
        data: {
            newUser
        }
    })
};


const updateUser = (req, res = response) => {
    const userId = parseInt(req.params.id);
    const body = req.body;

    const result = users.find(user => user.id === userId);
    if(!result) {
        return res.status(404).json({
            statusCode: 404,
            message: 'User not found'
        })
    }

    const newUser = {
        id: result.id,
        name: body.name || result.name,
        email: body.email || result.email,
        city: body.city || result.city,
        country: body.country || result.country,
        createdAt: result.createdAt,
        age: body.age || result.age
    }

    const itemIndex = users.findIndex(user => user.id === userId);
    users[itemIndex] = newUser;

    res.status(200).json({
        statusCode: 200,
        message: 'User updated',
        data: {
            newUser
        }
    })      
};


const deleteUser = (req, res = response) => {
    const userId = parseInt(req.params.id);
    const result = users.find(user => user.id === userId);
    if(!result) {
        return res.status(404).json({
            statusCode: 404,
            message: 'User not found'
        })
    }

    const itemIndex = users.findIndex(user => user.id === userId);
    users.splice(itemIndex, 1);

    res.status(200).json({
        statusCode: 200,
        message: 'User deleted'
    })
};

module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}